USE [Proyectos]
GO

/****** Object:  Table [dbo].[Proyectos]    Script Date: 16/9/2019 23:07:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Proyectos](
	[IDProyecto] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](120) NOT NULL,
	[Descripcion] [varchar](120) NOT NULL,
	[NombreResponsable] [varchar](120) NOT NULL,
	[EmailResponsable] [varchar](120) NOT NULL,
	[TelefonoResponsable] [varchar](12) NOT NULL,
	[IDLinea] [int] NOT NULL,
 CONSTRAINT [PK_Proyectos] PRIMARY KEY CLUSTERED 
(
	[IDProyecto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO