﻿using EscuelaNet.Dominio.Clientes;
using EscuelaNet.Dominio.SeedWoork;
using EsculaNet.Infraestructura.Clientes.EntityTypeConfigurations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsculaNet.Infraestructura.Clientes
{
    public class ClienteContext : DbContext, IUnitOfWork
    {
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<UnidadDeNegocio> Unidades { get; set; }
        public DbSet<Direccion> Direcciones { get; set; }
        public DbSet<Solicitud> Solicitudes { get; set; }
        public ClienteContext() : base("ClienteContext")
        {

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ClientesEntityTypeConfiguration());
            modelBuilder.Configurations.Add(new UnidadesDeNegocioEntityTypeConfiguration());
            modelBuilder.Configurations.Add(new DireccionesEntityTypeConfiguration());
            modelBuilder.Configurations.Add(new SolicitudesEntityTypeConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }
}
