﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using EscuelaNet.Aplicacion.Clientes.QueryModels;

namespace EscuelaNet.Aplicacion.Clientes.QueryServices
{
    public class ClientesQuery : IClientesQuery
    {

        private string _connectionString;

        public ClientesQuery(string connectionString)
        {
            _connectionString = connectionString;
        }


        public ClientesQueryModel GetCliente(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<ClientesQueryModel>(
                    @"
                    SELECT c.IDCliente as ID, c.RazonSocial as RazonSocial,
                    c.Email as Email, c.Categoria as Categoria
                    FROM Clientes as c
                    WHERE IDCliente = @id                   
                    ", new { id = id }
                    ).FirstOrDefault();
            }
        }

        public List<ClientesQueryModel> ListCliente()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<ClientesQueryModel>(
                    @"
                    SELECT c.IDCliente as ID, c.RazonSocial as RazonSocial,
                    c.Email as Email, c.Categoria as Categoria
                    FROM Clientes as c
                    "
                    ).ToList();
            }
        }
    }
}
