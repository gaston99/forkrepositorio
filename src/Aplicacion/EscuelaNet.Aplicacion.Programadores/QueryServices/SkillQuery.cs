﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using EscuelaNet.Aplicacion.Programadores.QueryModels;

namespace EscuelaNet.Aplicacion.Programadores.QueryServices
{
    public class SkillQuery : ISkillQuery
    {

        private string _connectionString;
        public SkillQuery(string connectionString)
        {
            _connectionString = connectionString;
        }
        public SkillQueryModel GetSkill(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<SkillQueryModel>(
                    @"
                    SELECT s.IdSkill as Id, s.Descripcion as Descripcion, s.Grados as Grados
                    FROM Skill as s
                    WHERE s.IdSkill = @id
                    ", new { id = id }
                    ).FirstOrDefault();
            }
        }

        public List<SkillQueryModel> ListSkill()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<SkillQueryModel>(
                    @"
                    SELECT s.IdSkill as Id, s.Descripcion as Descripcion, s.Grados as Grados
                    FROM Skill as s
                    ORDER BY s.Grados DESC
                    "
                    ).ToList();
            }
        }
    }
}
