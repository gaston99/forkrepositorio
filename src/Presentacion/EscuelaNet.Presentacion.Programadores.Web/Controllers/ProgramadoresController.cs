﻿using EscuelaNet.Aplicacion.Programadores.Commands;
using EscuelaNet.Aplicacion.Programadores.QueryServices;
using EscuelaNet.Dominio.Programadores;
using EscuelaNet.Presentacion.Programadores.Web.Infraestructura;
using EscuelaNet.Presentacion.Programadores.Web.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Programadores.Web.Controllers
{
    public class ProgramadoresController : Controller
    {
        private IProgramadorRepository _repositorio;
        private ISkillRepository _repositorioSKill;
        private IProgramadorQuery _programadorQuery;
        private ISkillQuery _skillQuery;
        private IMediator _mediator;

        public ProgramadoresController(IProgramadorRepository programadorRepository, ISkillRepository skillRepository,
           IProgramadorQuery programadorQuery, ISkillQuery skillQuery , IMediator mediator)
        {
            _repositorio = programadorRepository;
            _repositorioSKill = skillRepository;
            _programadorQuery = programadorQuery;
            _skillQuery = skillQuery;
            _mediator = mediator;

        }
        // GET: Programadores
        public ActionResult Index()
        {
            //var programador = _repositorio.ListProgramador();
            var programador = _programadorQuery.ListProgramador();
            var model = new ProgramadoresIndexModel()
            {
                Titulo = "Programadores",
                Programadores = programador
            };

             return View(model);
        }
        public ActionResult New()
        {
            var model = new NuevoProgramadorModel();
            return View(model);
        }

        // POST: Programadores/Create
        [HttpPost]
        public async Task<ActionResult> New(NuevoProgramadorCommand model)
        {
            var exito = await _mediator.Send(model);
            if (exito.Succes)
            {
                TempData["success"] = "Programador creado";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevoProgramadorModel()
                {
                    Nombre = model.Nombre,
                    Apellido = model.Apellido,
                    Legajo = model.Legajo,
                    Dni = model.Dni,
                    Rol = model.Rol,
                    FechaNacimiento = model.FechaNacimiento,
                    Disponibilidad = model.Disponibilidad
                };
                return View(modelReturn);
            }
        }

        // GET: Programadores/Edit/5
        public ActionResult Edit(int id)
        {
            var programador = _programadorQuery.GetProgramador(id);

            var model = new NuevoProgramadorModel()
            {
                IdProgramadores = id,
                Nombre = programador.Nombre,
                Apellido = programador.Apellido,
                Legajo = programador.Legajo,
                Dni = programador.Dni,
                Rol = programador.Rol,
                FechaNacimiento = programador.FechaNacimiento,
                Disponibilidad = programador.Disponibilidad
            };
            return View(model);
        }

        // POST: Programadores/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(UpdateProgramadorCommand model)
        {
            var exito = await _mediator.Send(model);
            if (exito.Succes)
            {
                TempData["success"] = "Programador Editado";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevoProgramadorModel()
                {
                    Nombre = model.Nombre,
                    Apellido = model.Apellido,
                    Legajo = model.Legajo,
                    Dni = model.Dni,
                    Rol = model.Rol,
                    FechaNacimiento = model.FechaNacimiento,
                    Disponibilidad = model.Disponibilidad
                };
                return View(modelReturn);

            }
        }

        // GET: Programadores/Delete/5
        public ActionResult Delete(int id)
        {
            var programador = _programadorQuery.GetProgramador(id);

            var model = new NuevoProgramadorModel()
            {
                IdProgramadores = id,
                Nombre = programador.Nombre,
                Apellido = programador.Apellido,
                Legajo = programador.Legajo,
                Dni = programador.Dni,
                Rol = programador.Rol,
                FechaNacimiento = programador.FechaNacimiento,
                Disponibilidad = programador.Disponibilidad
            };
            return View(model);
        }

        // POST: Programadores/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(DeleteProgramadorCommand model)
        {
            var exito = await _mediator.Send(model);
            if (exito.Succes)
            {
                TempData["success"] = "Programador Borrado";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevoProgramadorModel()
                {
                    Nombre = model.Nombre,
                    Apellido = model.Apellido,
                    Legajo = model.Legajo,
                    Dni = model.Dni,
                    Rol = model.Rol,
                    FechaNacimiento = model.FechaNacimiento,
                    Disponibilidad = model.Disponibilidad
                };
                return View(modelReturn);
            }
        }

        public ActionResult Skills(int id)
        {
            var programador = _repositorio.GetProgramador(id);
            var model = new ProgramadorSkillModel()
            {
                Programador = programador,
                Skills = programador.Skills.ToList()
            };
            return View(model);
        }
        public ActionResult DeleteSkills(int id, int idProgramador)
        {
            var programdorBuscado = _repositorio.GetProgramador(idProgramador);
            var skillBuscado = programdorBuscado.Skills.First(s => s.ID == id);

            var model = new NuevoProgramadorSkillModel()
            {
                DescripcionSkill = skillBuscado.Descripcion + " " + skillBuscado.Grados,
                IdProgramador = idProgramador,
                NombreProgramador = programdorBuscado.Nombre,
                IdSkill = id,
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult DeleteSkills(NuevoProgramadorSkillModel model)
        {
            try
            {
                var programadorBuscado = _repositorio.GetProgramador(model.IdProgramador);
                var skillBuscado = programadorBuscado.Skills.First(s => s.ID == model.IdSkill);
                programadorBuscado.RemoveSkill(skillBuscado);

                _repositorio.Update(programadorBuscado);
                _repositorio.UnitOfWork.SaveChanges();

                TempData["success"] = "Conocimiento borrado";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }
        public ActionResult NewSkill(int id)
        {
            var programador = _repositorio.GetProgramador(id);
            var skill = _skillQuery.ListSkill();

            var model = new NuevoProgramadorSkillModel()
            {
                IdProgramador = id,
                NombreProgramador = programador.Nombre,
                Skills = skill
            };
            return View(model);
        }

        // POST: Instructor/New
        [HttpPost]
        public ActionResult NewSkill(NuevoProgramadorSkillModel model)
        {
            try
            {
                var programadorBuscado = _repositorio.GetProgramador(model.IdProgramador);
                var skillBuscado = _repositorioSKill.GetSkill(model.IdSkill);

                if (!programadorBuscado.Skills.Contains(skillBuscado))
                {
                    programadorBuscado.PushConocimiento(skillBuscado);

                    _repositorio.Update(programadorBuscado);
                    _repositorio.UnitOfWork.SaveChanges();

                    TempData["success"] = "Skill Agregado";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["error"] = "Ya se encuentra el Skill";
                    return RedirectToAction("NewSkill", new { model.IdProgramador });
                }

            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }

        }
    }
}
