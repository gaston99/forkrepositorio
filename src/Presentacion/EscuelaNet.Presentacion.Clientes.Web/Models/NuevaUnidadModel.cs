﻿using EscuelaNet.Dominio.Clientes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Clientes.Web.Models
{
    public class NuevaUnidadModel
    {
        public string Titulo { get; set; }

        public int IdCliente { get; set; }

        public int IdUnidad { get; set; }

        public string RazonSocial { get; set; }

        public string ResponsableDeUnidad { get; set; }

        public string Cuit { get; set; }

        public string EmailResponsable { get; set; }

        public string TelefonoResponsable { get; set; }

        

        

    }
}