﻿using EscuelaNet.Dominio.Clientes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Clientes.Web.Models
{
    public class NuevoClienteModel
    {

        public int Id { get; set; }

        public string RazonSocial { get; set; }

        public string Email { get; set; }

        public Categoria Categoria { get; set; }

        

    }
}