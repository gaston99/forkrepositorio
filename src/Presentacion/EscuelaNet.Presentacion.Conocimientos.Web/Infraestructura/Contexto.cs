﻿using EscuelaNet.Dominio.Conocimientos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Conocimientos.Web.Infraestructura
{
    public sealed class Contexto
    {
        private static Contexto _instancia = new Contexto();
        public List<Categoria> Categorias { get; set; }
        public List<Conocimiento> Conocimientos { get; set; }

        private Contexto()
        {

            this.Categorias = new List<Categoria>();

            Categorias.Add(new Categoria( "Programación", "lorem ipsum dolor sit amet consectetur adipiscing elit"));
            var Conocimiento = new Conocimiento("Java");
            this.Categorias[0].AgregarConocimiento(Conocimiento);
            //Conocimiento.AgregarAsesor("Rosa", "Belmonte", "Frances", "Uruguay");

            var Conocimiento2 = new Conocimiento(".NET");
            this.Categorias[0].AgregarConocimiento(Conocimiento2);
            //Conocimiento2.AgregarAsesor("Solana", "Campo Diaz","Ruso", "Argentina");


            Categorias.Add(new Categoria( "Seguridad Informática", "lorem ipsum dolor sit amet consectetur adipiscing elit"));
            var Conocimiento3 = new Conocimiento("Seguridad Hardware");
            this.Categorias[1].AgregarConocimiento(Conocimiento3);
            //Conocimiento3.AgregarAsesor("Luba", "Jarife", "Español", "Bolivia");

            var Conocimiento4 = new Conocimiento("Seguridad de Software");
            this.Categorias[1].AgregarConocimiento(Conocimiento4);
            //Conocimiento4.AgregarAsesor("Belen", "Godoy", new DateTime(1998, 1, 1), "Frances", "Mexico");


            Categorias.Add(new Categoria( "Base de Datos", "lorem ipsum dolor sit amet consectetur adipiscing elit"));
            var Conocimiento5 = new Conocimiento("SQL");
            this.Categorias[2].AgregarConocimiento(Conocimiento5);
            //Conocimiento5.AgregarAsesor("Juan Pablo", "Ceballos", new DateTime(1993, 6, 7), "Ingles", "Argentina");

           
        }

        public static Contexto Instancia
        {
            get
            {
                return _instancia;
            }
        }

    }
}
